// Initialise the current stream acceptance
let binaryStringAccepted = false;
// Initialise the custom Network data taken from Vis Network
let ndfaFromNetwork = {};
// Initialise the sequence of tasks that the player needs to perform
let tasks = [];

// Initialise custom dataSet from Vis network (every time a new graph is generated)
function initialiseNetwork() {
  ndfaFromNetwork = createNDFAfromNetwork(nodes, edges);
  console.log(ndfaFromNetwork);
}

// Get the number of edges of a specific node
function getEdgesOfNode(nodeId) {
  return edges.get().filter(function (edge) {
    return edge.from === nodeId;
  });
}

// Creating network to traverse from the vis.js network
function createNDFAfromNetwork(nodes, edges) {
  // outer object which contains the inner objects of the network 
  let outerObj = {};
  // counter to visit all the edges (twice the amount of nodes)
  let edgeCounter = 1;
  // visit all nodes
  for (index = 1; index < nodes.length; index++) {

    // getting the node label information
    let tempString = nodes.get(index).label;
    // inner object initialisation
    let innerObj = {};
    // set the inner object name to the node label
    outerObj[tempString] = innerObj;
    // Number of edges to from the current node
    let numberOfEdges = getEdgesOfNode(index);

    // The current node only has 1 edge
    if (numberOfEdges.length == 1) {
      // Add one propertie to the inner Obj. Edge 0 goes to next node label and edge b goes to next next node label
      innerObj[edges.get(edgeCounter).val] = [nodes.get(edges.get(edgeCounter).to).label];
      // Add acceted label property to see if the node accepts the input
      innerObj.isAccept = nodes.get(index).accepted;
      // Add current node id property to keep track of where we are
      innerObj.id = nodes.get(index).id;
      // increment the counter by two to cover all the edges (2 each node)
      edgeCounter += 1;
    } // The current node has 2 edges
    else if (numberOfEdges.length == 2) {
      // Add one propertie to the inner Obj. Edge 0 goes to next node label and edge b goes to next next node label
      innerObj[0] = [nodes.get(edges.get(edgeCounter).to).label];
      innerObj[1] = [nodes.get(edges.get(edgeCounter + 1).to).label];
      // Add acceted label property to see if the node accepts the input
      innerObj.isAccept = nodes.get(index).accepted;
      // Add current node id property to keep track of where we are
      innerObj.id = nodes.get(index).id;
      // increment the counter by two to cover all the edges (2 each node)
      edgeCounter += 2;
    } // The current node has 3 edges
    else if (numberOfEdges.length == 3) {
      // Add one propertie to the inner Obj. Edge 0 goes to next node label and edge b goes to next next node label
      innerObj[0] = [nodes.get(edges.get(edgeCounter).to).label];
      innerObj[1] = [nodes.get(edges.get(edgeCounter + 1).to).label];
      // increment the counter by two to cover all the edges (2 each node)
      edgeCounter += 2;
      if ((edges.get(edgeCounter)).val == 1) {
        innerObj[1].push(nodes.get(edges.get(edgeCounter).to).label);
      } else {
        innerObj[0].push(nodes.get(edges.get(edgeCounter).to).label);
      }
      // Add acceted label property to see if the node accepts the input
      innerObj.isAccept = nodes.get(index).accepted;
      // Add current node id property to keep track of where we are
      innerObj.id = nodes.get(index).id;
      // Increment the counter by one 
      edgeCounter += 1;
    }
  }
  // Getting label 'A', first node
  outerObj.startState = nodes.get(1).label;
  return outerObj;
}