// General UI controls 
$(document).ready(() => {
  // Fade out and fade in animation for the intro container 
  $('#begin').click(() => {
    $('#introContainer').fadeOut(2000, () => {
      $('#main-container-grid').fadeIn(2000);
      $('#main-container-grid').css('display', 'grid');
      // Stop generating random values for the intro container
      clearInterval(introInterval);
      // Load the main app
      initInterval = true;
      $(".modal").modal("show");
    });
  });
});