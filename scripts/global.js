// Global variables
let introInterval;
let initInterval = false;
let currentString = '';
let currentTutorialCount = 0;
let graphPositioned = false;
var network;
var nodes;
var edges;
var data;
var player;
var tutorial;
var currentList = $('.array-element');


$(document).ready(() => {

  // Tutorial Object, has text to display and a function to change the state of the content
  tutorial = [{
      text: "Welcome, I'll be your Task Manager and Guide <button class = 'button-next'> Next</button> <button class = 'button-skip'>Skip</button>",
      set: function () {
        $('#info-container').addClass('non-active');
        $('#canvas-container').addClass('non-active');
        $('#info-container-transparent').css('display', 'block');
        $('#canvas-container-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can find information about the Graph <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-text').addClass('highlightBorder');
        $('#info-container-buttons-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can generate new random Graphs and Strings <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-text').removeClass('highlightBorder');
        $('#info-container-buttons').addClass('highlightBorder');
        $('#info-container-text-transparent').css('display', 'block');
        $('#info-container-buttons-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can manipulate the Graph and check the String Display <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-buttons').removeClass('highlightBorder');
        $('#canvas-container').addClass('highlightBorder');
        $('#info-container-buttons-transparent').css('display', 'block');
        $('#canvas-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Finally you can use the [Help] button to review information <button class = 'button-next'>Next</button>",
      set: function () {
        $('#canvas-container').removeClass('highlightBorder');
        $('#help').addClass('highlightBorder');
        $('#canvas-container-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Great, I think you are ready to go !<button class = 'button-next'>Begin</button>",
      set: function () {
        $('#help').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Position and organise the Graph so that it is clear to you <button class = 'button-next'>Done</button>",
      set: function () {
        $('#help').removeClass('highlightBorder');
        $('#canvas-container').removeClass('non-active');
        $('#canvas-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Check the String Manager and make your first Transition to the next Node",
      set: function () {
        $('#info-container').removeClass('non-active');
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-buttons-transparent').css('display', 'none');
        $('#info-container-text-transparent').css('display', 'none');
        $('#canvas-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    }
  ];

  // Setting the first tutorial task
  tutorial[currentTutorialCount].set();

  // Increment tutorial task on button click 
  $('#suggestion').on('click', '.button-next', () => {
    // Last tutorial task before the game 
    if (currentTutorialCount == 6) {
      graphPositioned = true;
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    } else {
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    }
  });
  //Jump the tutorial section 
  $('#suggestion').on('click', '.button-skip', () => {
    currentTutorialCount = 6;
    tutorial[currentTutorialCount].set();
  });
  // Toggle the Graph Legenda on help button click 
  $('#help').on('click', () => {
    $(".modal-title").html('Graph Legend');
    $(".modal-body").html('<h3 style="margin-top:30px">Actions</h3><p> The <strong>Graph</strong> is fully interactive and you can :</p> <p><strong>Position the Graph :</strong> Click one of the edges and drag</p> <p><strong>Zoom in and Out :</strong> Use the mouse wheel</p> <p><strong>Select Nodes : </strong> Click the desidered node</p> <p style="margin-bottom:40px"><strong>Drag Nodes :</strong> Click and drag the desidered node</p> <h3>Nodes</h3> <p style = "font-size:20px"> Current Node <span id="info-state">A</span></p> <p style = "font-size:20px"> Nodes with a <strong>Reject State</strong> <span id="reject-state">A</span></p><p style = "font-size:20px"> Nodes with an <strong>Accept State</strong> <span id="accept-state">A</span></p>  <h3>Edges</h3> <p style = "font-size:20px"> Edge/Transition <img src = "images/arrow.gif" id ="arrow"/> </p> <p style = "font-size:20px"> Edge Value <img src = "images/arrow_label.gif" id ="arrow-label"/> </p> <h3>String Display</h3><p> The <strong>String Display</strong> highlights the next expected transition:</p><p><img src = "images/string_manager.gif" id ="string-manager"/> </p> ');
    $(".modal").modal("show");
  });
});

// Reset the classeson the string array object for computation 
function resetArrayClasses() {
  for (let index = 0; index < 6; index++) {
    $(currentList[index]).removeClass('array_done');
    $(currentList[index]).removeClass('hide_element');
    $(currentList[index]).addClass('array-element');
    $(currentList[index]).html('<span class="element">' + 0 + '</span>');
    console.log('remove array-done ');
  }
}