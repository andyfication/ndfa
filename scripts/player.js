class Player {
  constructor(nodes, network, custom_network, currentString) {
    // Current network to update the node colours 
    this.network = network;
    // List of nodes to analyze
    this.nodes = nodes;
    // Node id, starting from the first node 
    this.currentId = 1;
    // Current String we are playing with
    this.currentString = currentString;
    // List we are playing with
    this.listToPlay = $('.array-element');
    // Copy of the Vis network structure
    this.custom_network = custom_network;
    // String Counter
    this.currentStringCounter = 0;
    // First char of the string
    this.currentChar = this.currentString[0];
    // Keep track of the tasks
    this.currentTask = 0;
  }
  // Initialise with task number one end highlighting the entry node
  initStartingPosition() {
    // Reset current info state to current node lable 
    $('#info-state').html("A");
    // First char of the string
    this.currentChar = this.currentString[0];
    // Reset Graph Interaction 
    var options = {
      interaction: {
        selectable: true,
        hover: true
      }
    }
    this.network.setOptions(options);
    // Check if we have a possible path to follow 
    this.checkIfNoPath(this.currentId);
    // reset the current id and task 
    this.currentId = 1;
    this.currentTask = 0;
    // Reset the graph colours
    for (let index = 1; index < this.nodes.length; index++) {
      if (this.nodes.get(index).accepted) {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: '#6fdc6f',
            highlight: {
              background: 'rgb(40,40,40)',
              border: '#6fdc6f'
            }
          }
        });
      } else {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: 'white',
            highlight: {
              background: 'rgb(40,40,40)',
              border: 'white'
            }
          }
        });
      }
    }
    this.nodes.update({
      id: this.currentId,
      color: {
        border: 'white',
        background: '#ff9900'
      }
    });
    this.initList(); // Initialise visual for nodes and graph 
  }

  // Function that checks on button 'yes' at the end of the string 
  acceptStringAnswer(e) {
    if (e.target.value == this.nodes.get(this.currentId).accepted.toString()) {
      $('#suggestion').html('Well Done, The current String is Accepted');
      $('.answer').css('display', 'none');
      $('#info-container-text').removeClass('highlightBorder');
      $('#info-container-buttons').addClass('highlightBorder');
    } else
      $('#suggestion').html('Not Really, Can the String be accepted ?');
  }
  // Function that checks on button 'no' at the end of the string 
  rejectStringAnswer(e) {
    if (e.target.value == this.nodes.get(this.currentId).accepted.toString()) {
      $('#suggestion').html('Well Done, The current String is Rejected');
      $('.answer').css('display', 'none');
      $('#info-container-text').removeClass('highlightBorder');
      $('#info-container-buttons').addClass('highlightBorder');
    } else
      $('#suggestion').html('Not Really, Can the String be accepted ?');
  }
  // Function that checks on button 'yes' when no path from current node 
  acceptStringAnswerNoPath(e) {
    if (e.target.value = "true") {
      $('#suggestion').html('Not Really, Can the String be accepted ?');
    }
  }
  // Function that checks on button 'no' when no path from current node 
  rejectStringAnswerNoPath(e) {
    if (e.target.value = "false") {
      $('#suggestion').html('Well Done, The current String is Rejected');
      $('#info-container-buttons').addClass('highlightBorder');
      $('#info-container-text').removeClass('highlightBorder');
      $('.answer-no-path').css('display', 'none');
    }
  }

  checkClickedNode(node) {
    // Number of edges to from the current node
    let numberOfEdges = getEdgesOfNode(this.currentId);
    // The current node has one edge
    if (numberOfEdges.length == 1) {
      // Check if the node clicked is somehow linked with the current node
      if (Object.values(this.custom_network[this.nodes.get(this.currentId).label])[0][0] != node[0].label) {
        // No linked nodes found------------------------------------
        // display no linked text 
        $('#suggestion').html('Not Really, Check your current State and the String Manager');
        $('#info-state').html(this.nodes.get(this.currentId).label);
        // update current node design to red as this is not the correct node
        console.log(node[0].id + 'red')
        if (node[0].accepted) {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: '#6fdc6f',
              highlight: {
                background: 'red',
                border: '#6fdc6f'
              }
            }
          });
        } else {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: 'white',
              highlight: {
                background: 'red',
                border: 'white'
              }
            }
          });
        }
      } else {
        // Found possible connection------------------------------------------------------------
        // Check the current string char and check if correspond to the label
        if (this.currentChar == Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[0]) {
          this.currentId = node[0].id;
          this.currentTask += 1;
          this.currentChar = this.currentString[this.currentTask];
          $('#suggestion').html('Well Done, Now make a Transition to the next Node');
          $('#info-state').html(this.nodes.get(this.currentId).label);
          // clear the other nodes design
          for (let index = 1; index < this.nodes.length; index++) {
            if (this.nodes.get(index).accepted) {
              this.nodes.update({
                id: index,
                borderWidth: 2,
                color: {
                  background: 'rgb(40,40,40)',
                  highlight: {
                    border: '#6fdc6f',
                    background: 'rgb(40,40,40)'
                  }
                }
              });
            } else {
              this.nodes.update({
                id: index,
                borderWidth: 2,
                color: {
                  background: 'rgb(40,40,40)',
                  highlight: {
                    background: 'rgb(40,40,40)'

                  }
                }
              });
            }
          }
          // clear first node design 
          this.nodes.update({
            id: 1,
            color: {
              border: 'white',
              background: 'rgb(40,40,40)'
            }
          });
          // update current node design 
          this.nodes.update({
            id: this.currentId,
            color: {
              background: '#ff9900',
              highlight: {
                background: '#ff9900'

              }

            }
          });
          // Update string character to show the current highlighted string character  
          this.updateList();
          // Check if we have a possible path to follow 
          this.checkIfNoPath(this.currentId);

        } else {
          // Clicked on wrong node even if linked with current node---------------------------------------------
          $('#suggestion').html('Not Really, Check your current State and the String Manager');
          $('#info-state').html(this.nodes.get(this.currentId).label);
          // update current node design to red as this is not the correct node
          console.log(node[0].id + 'red')
          if (node[0].accepted) {
            this.nodes.update({
              id: node[0].id,
              color: {
                border: '#6fdc6f',
                highlight: {
                  background: 'red',
                  border: '#6fdc6f'
                }
              }
            });
          } else {
            this.nodes.update({
              id: node[0].id,
              color: {
                border: 'white',
                highlight: {
                  background: 'red',
                  border: 'white'
                }
              }
            });
          }
        }
      }
    }
    // The current node has two edges
    else if (numberOfEdges.length == 2) {
      console.log(this.custom_network);
      // Check if the node clicked is somehow linked with the current node
      if (this.custom_network[this.nodes.get(this.currentId).label][0][0] != node[0].label && this.custom_network[this.nodes.get(this.currentId).label][1][0] != node[0].label) {
        // No link found with current node----------------------------------- 
        $('#suggestion').html('Not Really, Check your current State and the String Manager');
        $('#info-state').html(this.nodes.get(this.currentId).label);
        // update current node design to red as this is not the correct node
        console.log(node[0].id + 'red')
        if (node[0].accepted) {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: '#6fdc6f',
              highlight: {
                background: 'red',
                border: '#6fdc6f'
              }
            }
          });
        } else {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: 'white',
              highlight: {
                background: 'red',
                border: 'white'
              }
            }
          });
        }
      } else {
        // Check the current string char and check if correspond to the label
        if (node[0].label == Object.values(this.custom_network[this.nodes.get(this.currentId).label])[this.currentChar][0]) {
          // Found the correct linked node-------------------------------------------
          this.currentId = node[0].id;
          this.currentTask += 1;
          this.currentChar = this.currentString[this.currentTask];
          $('#suggestion').html('Well Done, Now make a Transition to the next Node');
          $('#info-state').html(this.nodes.get(this.currentId).label);
          // clear the other nodes design
          for (let index = 1; index < this.nodes.length; index++) {
            if (this.nodes.get(index).accepted) {
              this.nodes.update({
                id: index,
                borderWidth: 2,
                color: {
                  background: 'rgb(40,40,40)',
                  highlight: {
                    border: '#6fdc6f',
                    background: 'rgb(40,40,40)'
                  }
                }
              });
            } else {
              this.nodes.update({
                id: index,
                borderWidth: 2,
                color: {
                  background: 'rgb(40,40,40)',
                  highlight: {
                    background: 'rgb(40,40,40)'

                  }
                }
              });
            }
          }
          // clear first node design 
          this.nodes.update({
            id: 1,
            color: {
              border: 'white',
              background: 'rgb(40,40,40)'
            }
          });
          // update current node design 
          this.nodes.update({
            id: this.currentId,
            color: {
              background: '#ff9900',
              highlight: {
                background: '#ff9900'

              }

            }
          });
          // Update string character to show the current highlighted string character  
          this.updateList();
          // Check if we have a possible path to follow 
          this.checkIfNoPath(this.currentId);
        } else {
          // Wrong node, linked but no right label----------------------------------------------------
          $('#suggestion').html('Not Really, Check your current State and the String Manager');
          $('#info-state').html(this.nodes.get(this.currentId).label);
          // update current node design to red as this is not the correct node
          console.log(node[0].id + 'red')
          if (node[0].accepted) {
            this.nodes.update({
              id: node[0].id,
              color: {
                border: '#6fdc6f',
                highlight: {
                  background: 'red',
                  border: '#6fdc6f'
                }
              }
            });
          } else {
            this.nodes.update({
              id: node[0].id,
              color: {
                border: 'white',
                highlight: {
                  background: 'red',
                  border: 'white'
                }
              }
            });
          }
        }
      }

    } // The current node has three edges
    else if (numberOfEdges.length == 3) {
      // Have we found a label match from the node we are coming from
      let foundMatch = false;
      // We did not find the match in the first pass
      let secondSearch = true;
      // Check if the node clicked is somehow linked with the current node
      for (let index = 0; index < this.custom_network[this.nodes.get(this.currentId).label][0].length; index++) {
        if (this.custom_network[this.nodes.get(this.currentId).label][0][index] != node[0].label) {
          // Not found yet on the first array
          foundMatch = false;
        } else {
          // Found on the first pass, no need to search the second pass
          foundMatch = true;
          secondSearch = false;
          break;
        }
      }
      // Not found on first pass, start search on second pass 
      if (secondSearch) {
        for (let index = 0; index < this.custom_network[this.nodes.get(this.currentId).label][1].length; index++) {
          if (this.custom_network[this.nodes.get(this.currentId).label][1][index] != node[0].label) {
            // Not found on second pass
            foundMatch = false;
          } else {
            // Found on second pass 
            foundMatch = true;
            break;
          }
        }
      }
      // If there is a possible match 
      if (foundMatch) {
        for (let index = 0; index < Object.values(this.custom_network[this.nodes.get(this.currentId).label])[this.currentChar].length; index++) {
          // Check if it is within the object property based on the string input 
          if (node[0].label == Object.values(this.custom_network[this.nodes.get(this.currentId).label])[this.currentChar][index]) {
            // found
            this.currentId = node[0].id;
            this.currentTask += 1;
            this.currentChar = this.currentString[this.currentTask];
            $('#suggestion').html('Well Done, Now make a Transition to the next Node');
            $('#info-state').html(this.nodes.get(this.currentId).label);
            // clear the other nodes design
            for (let index = 1; index < this.nodes.length; index++) {
              if (this.nodes.get(index).accepted) {
                this.nodes.update({
                  id: index,
                  borderWidth: 2,
                  color: {
                    background: 'rgb(40,40,40)',
                    highlight: {
                      border: '#6fdc6f',
                      background: 'rgb(40,40,40)'
                    }
                  }
                });
              } else {
                this.nodes.update({
                  id: index,
                  borderWidth: 2,
                  color: {
                    background: 'rgb(40,40,40)',
                    highlight: {
                      background: 'rgb(40,40,40)'

                    }
                  }
                });
              }
            }
            // clear first node design 
            this.nodes.update({
              id: 1,
              color: {
                border: 'white',
                background: 'rgb(40,40,40)'
              }
            });
            // update current node design 
            this.nodes.update({
              id: this.currentId,
              color: {
                background: '#ff9900',
                highlight: {
                  background: '#ff9900'

                }

              }
            });
            // Update string character to show the current highlighted string character  
            this.updateList();
            // Check if we have a possible path to follow 
            this.checkIfNoPath(this.currentId);
            break;
          } else {
            // not found--------------------------------------------------------------------------------
            $('#suggestion').html('Not Really, Check your current State and the String Manager');
            $('#info-state').html(this.nodes.get(this.currentId).label);
            // update current node design to red as this is not the correct node
            console.log(node[0].id + 'red')
            if (node[0].accepted) {
              this.nodes.update({
                id: node[0].id,
                color: {
                  border: '#6fdc6f',
                  highlight: {
                    background: 'red',
                    border: '#6fdc6f'
                  }
                }
              });
            } else {
              this.nodes.update({
                id: node[0].id,
                color: {
                  border: 'white',
                  highlight: {
                    background: 'red',
                    border: 'white'
                  }
                }
              });
            }

          }
        }
      } else {
        // No linked node founds when node has 3 edges-------------------------------------------------------------
        $('#suggestion').html('Not Really, Check your current State and the String Manager');
        $('#info-state').html(this.nodes.get(this.currentId).label);
        // update current node design to red as this is not the correct node
        console.log(node[0].id + 'red')
        if (node[0].accepted) {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: '#6fdc6f',
              highlight: {
                background: 'red',
                border: '#6fdc6f'
              }
            }
          });
        } else {
          this.nodes.update({
            id: node[0].id,
            color: {
              border: 'white',
              highlight: {
                background: 'red',
                border: 'white'
              }
            }
          });
        }

      }
    }
  }

  // Check if we end up on a node with no possible transitions
  checkIfNoPath(nodeId) {
    if (this.currentTask != 6) { // Are we at the end of the string, if not 
      // Number of edges to from the current node
      let numberOfEdges = getEdgesOfNode(nodeId);
      if (numberOfEdges.length == 1) {
        if (parseInt(this.currentChar) != Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[0]) {
          //disable graph interaction
          var options = {
            interaction: {
              selectable: false,
              hover: false
            }
          }
          this.network.setOptions(options);
          // Pop up the message of no more path available 
          $('#suggestion').html('Can the Automaton accept this String ?');
          $('.answer-no-path').css('display', 'inline-block');
          $('#info-container-text').addClass('highlightBorder');
        }
      } else if (numberOfEdges.length == 2) {
        if (parseInt(this.currentChar) != Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[0] && parseInt(this.currentChar) != Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[1]) {
          console.log('No solution from here');
          //disable graph interaction
          var options = {
            interaction: {
              selectable: false,
              hover: false
            }
          }
          this.network.setOptions(options);
          // Pop up the message of no more path available 
          $('#suggestion').html('Can the Automaton accept this String ?');
          $('.answer-no-path').css('display', 'inline-block');
        }
      } else if (numberOfEdges.length == 3) {
        if (parseInt(this.currentChar) != Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[0] && parseInt(this.currentChar) != Object.keys(this.custom_network[this.nodes.get(this.currentId).label])[1]) {
          //disable graph interaction
          var options = {
            interaction: {
              selectable: false,
              hover: false
            }
          }
          this.network.setOptions(options);
          // Pop up the message of no more path available 
          $('#suggestion').html('Can the Automaton accept this String ?');
          $('.answer-no-path').css('display', 'inline-block');
        }
      }
    } else { // We are at the end of the string 
      $('#suggestion').html('Can the Automaton accept this String ?');
      $('#info-state').html(this.nodes.get(this.currentId).label);
      $('.answer').css('display', 'inline-block');
      $('#info-container-text').addClass('highlightBorder');
      // All visited characters in the list
      this.endOfList();
      // clear the other nodes design based on if they are labled accepted or not 
      for (let index = 1; index < this.nodes.length; index++) {
        if (this.nodes.get(index).accepted) {
          this.nodes.update({
            id: index,
            borderWidth: 2,
            color: {
              background: 'rgb(40,40,40)',
              highlight: {
                border: '#6fdc6f',
                background: 'rgb(40,40,40)'
              }
            }
          });
        } else {
          this.nodes.update({
            id: index,
            borderWidth: 2,
            color: {
              background: 'rgb(40,40,40)',
              highlight: {
                background: 'rgb(40,40,40)'

              }
            }
          });
        }
      }
      // clear first node design 
      this.nodes.update({
        id: 1,
        color: {
          border: 'white',
          background: 'rgb(40,40,40)'
        }
      });
      // update current node design 
      this.nodes.update({
        id: this.currentId,
        color: {
          background: '#ff9900',
          border: "rgb(40,40,40)",
          highlight: {
            background: '#ff9900',
            border: "rgb(40,40,40)"
          }
        }
      });
      // We have reached the end, disable graph interaction
      var options = {
        interaction: {
          selectable: false,
          hover: false
        }
      }
      this.network.setOptions(options);
    }
  }
  // Initialise the current string array with the right colours interaction 
  initList() {
    // Remove Class from the array items 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array_done');
    }
    // Add Class from the array items
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).addClass('array-element');
    }
    // Assign list to array 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).html('<span class="element">' + this.currentString[index] + '</span>');
    }
    // Hide all elements except the first one on te list
    for (let index = 1; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).addClass('hide_element');
      $(this.listToPlay[index]).removeClass('array-element');
    }

  }
  // Update the list colours
  updateList() {
    // reset colours 
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array-element');
      $(this.listToPlay[index]).addClass('hide_element');
    }
    // Add highlight on current task 
    $(this.listToPlay[this.currentTask]).removeClass('hide_element');
    $(this.listToPlay[this.currentTask]).addClass('array-element');
    // Add visited on previous task 
    $(this.listToPlay[this.currentTask - 1]).removeClass('hide_element');
    $(this.listToPlay[this.currentTask - 1]).removeClass('array-element');
    $(this.listToPlay[this.currentTask - 1]).addClass('array_done');
  }
  // All visited at the end of the string 
  endOfList() {
    for (let index = 0; index < this.listToPlay.length; index++) {
      $(this.listToPlay[index]).removeClass('array-element');
      $(this.listToPlay[index]).removeClass('hide_element');
      $(this.listToPlay[index]).addClass('array_done');
    }
  }
}