// This function generates a binary string with eiter a 1 or a 0
function generateNewString() {
  let stringLen = 6;
  let newString = '';
  for (let counter = 0; counter < stringLen; counter++) {
    let randVal = Math.random();
    if (randVal > 0.5) {
      newString += '1';
    } else
      newString += '0';
  }
  return newString;
}
// This function generates a new random Graph (between 4 and 6 nodes and twice the edges of the node number)
function generateNewGraph(probability) {
  // nodes variable
  nodes = new vis.DataSet();
  // edges variable
  edges = new vis.DataSet();
  // number of nodes based on probability 
  let numberOfNodes;
  // number of edges
  let numberOfEdges;
  // Edges id
  let edgeId = 1;
  // number of nodes aray
  let numberOfNodesArray = [];
  // Previous Random Value
  let prevRandom;
  // Accepted state 
  let acc;
  // Do we have a self loop
  let selfLoop = false;
  // Temporary label to identify double self loops
  let selfLoopLabel;
  // Only a maximum of a third of the number of nodes can have accept state. Minimum none.
  let maxFourAccNodes = false;
  let maxFiveAccNodes = false;
  let maxSixAccNodes = false;
  let maxSixAccNodesCounter = 0;
  // The graph can have either 4,5 or 6 nodes based on probability
  if (probability <= 0.3) {
    numberOfNodes = 4;
  } else if (probability > 0.3 && probability <= 0.6) {
    numberOfNodes = 5;
  } else {
    numberOfNodes = 6;
  }
  // Fill the array to keep track of the nodes id
  for (index = 1; index <= numberOfNodes; index++) {
    numberOfNodesArray.push(index);
  }
  // Construct the nodes network 
  for (index = 1; index <= numberOfNodes; index++) {
    let letters = ['A', 'B', 'C', 'D', 'E', 'F'];
    if (numberOfNodes == 4) {

      // Have max one accept node out of 4 nodes 
      if (!maxFourAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        console.log(acc);
        if (acc == true) {
          maxFourAccNodes = true;
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxFourAccNodes == false) {
        acc = true;
      }
    } else if (numberOfNodes == 5) {
      if (!maxFiveAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        if (acc == true) {
          maxFiveAccNodes = true;
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxFiveAccNodes == false) {
        acc = true;
      }
    } else if (numberOfNodes == 6) {
      if (!maxSixAccNodes) {
        acc = Math.random() >= 0.5 ? true : false;
        if (acc == true) {
          maxSixAccNodesCounter += 1;
          if (maxSixAccNodesCounter == 2) {
            maxSixAccNodes = true;
          }
        }
      } else {
        acc = false;
      }
      // Have at least one node with accept state
      if (index == numberOfNodes && maxSixAccNodes == false) {
        acc = true;
      }
    }
    // Initialise and add all the fields
    let temp_id = 0;
    let temp_label = '';
    let temp_accepted = false;
    if (index == 0) {
      temp_id = -1;
      temp_label = 'Entry Point';
      temp_accepted = false;
    } else {
      temp_id = index;
      temp_label = letters[index - 1];
      temp_accepted = acc;
    }
    nodes.add({
      id: temp_id,
      label: temp_label,
      accepted: temp_accepted
    });
  }
  numberOfEdges = numberOfNodes;

  // Construct the edges structure------------------------------------------
  for (let index = 0; index <= numberOfEdges; index++) {
    // Initialise variable 
    let temp_from;
    let temp_to;
    let temp_id;
    let temp_val;
    let temp_label;
    let tempFont = {}
    prevRandom = 0;
    selfLoop = false;

    // First edge has no label, entry edge
    if (index == 0) {
      temp_from = -1;
      temp_to = 1;
      temp_id = index;
      temp_val = -1;

      edges.add({
        from: temp_from,
        to: temp_to,
        id: temp_id,
        val: temp_val
      });

    } else {
      // Random value to add 1,2 or 3 edges to a specific node
      let randomChoice = Math.random();
      // Only add one edge to the current Node 
      if (randomChoice <= 0.3) {
        temp_from = index;
        temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
        temp_to = numberOfNodesArray[temp_current_Random];
        temp_label = Math.random() > 0.5 ? "1" : "0";
        temp_id = edgeId;
        temp_val = parseInt(temp_label);
        edgeId += 1;
        tempFont.size = 15;
        tempFont.background = '#ffffff';
        edges.add({
          from: temp_from,
          to: temp_to,
          label: temp_label,
          id: temp_id,
          val: temp_val,
          font: tempFont
        });
      } // Add two edges to the current Node 
      else if (randomChoice > 0.3 && randomChoice <= 0.6) {
        // There are two edges for every node 
        for (let edgeAmount = 0; edgeAmount < 2; edgeAmount++) {
          temp_from = index;
          temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
          // Make sure that no more than 1 edge with the same value goes to a specific node 
          if (temp_current_Random != prevRandom)
            temp_to = numberOfNodesArray[temp_current_Random];
          else {
            while (temp_current_Random == prevRandom) {
              temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
            }
            temp_to = numberOfNodesArray[temp_current_Random];
          }
          // Add the values to the edge properties 
          temp_label = edgeAmount.toString();
          temp_id = edgeId;
          temp_val = edgeAmount;
          edgeId += 1;
          tempFont.size = 15;
          tempFont.background = '#ffffff';
          prevRandom = temp_current_Random;
          edges.add({
            from: temp_from,
            to: temp_to,
            label: temp_label,
            id: temp_id,
            val: temp_val,
            font: tempFont
          });
        }

      } // Add three edges to the current Node 
      else if (randomChoice > 0.6) {
        // There are three edges for every node 
        for (let edgeAmount = 0; edgeAmount < 3; edgeAmount++) {
          temp_from = index;
          temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
          // Make sure that no more than 1 edge with the same value goes to a specific node 
          if (temp_current_Random != prevRandom)
            temp_to = numberOfNodesArray[temp_current_Random];
          else {
            while (temp_current_Random == prevRandom) {
              temp_current_Random = Math.floor(Math.random() * numberOfNodesArray.length);
            }
            temp_to = numberOfNodesArray[temp_current_Random];
          }
          // If we have a self loop record it 
          if (temp_from == temp_to && edgeAmount != 2) {
            selfLoop = true;
            selfLoopLabel = edgeAmount.toString();
          }
          if (edgeAmount == 2) {
            // If there was a loop already add the label to it 
            if (selfLoop && (temp_from == temp_to)) {
              temp_label = Math.random() > 0.5 ? "1" : "0";
              temp_val = parseInt(temp_label);
              // Do not display the same label if equal 
              if (temp_label != selfLoopLabel)
                temp_label = temp_label + "" + selfLoopLabel;
              else
                temp_label = selfLoopLabel;
            } else if (!selfLoop && temp_from == temp_to) {
              temp_label = Math.random() > 0.5 ? "1" : "0";
              temp_val = parseInt(temp_label);
            } else {
              temp_label = Math.random() > 0.5 ? "1" : "0";
              temp_val = parseInt(temp_label);
            }

            temp_id = edgeId;
          } else {
            temp_label = edgeAmount.toString();
            temp_id = edgeId;
            temp_val = edgeAmount;
          }
          // Add values to the edge properties 
          edgeId += 1;
          tempFont.size = 15;
          tempFont.background = '#ffffff';
          prevRandom = temp_current_Random;
          edges.add({
            from: temp_from,
            to: temp_to,
            label: temp_label,
            id: temp_id,
            val: temp_val,
            font: tempFont
          });
        }

      }
    }
  }
  // Create the Data Set
  data = {
    nodes: nodes,
    edges: edges
  };
  console.log(edgeId);
  return data;
}