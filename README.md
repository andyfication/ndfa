This is my repo for the Non-Deterministic Finite Automaton project.


I worked with Goldsmiths, University of London and Coursera to develop some online interactive learning tools. The Non-Deterministic Finite Automaton is among these.

The NDFA is an interactive simulation made in HTML5,CSS3 and javaScript.

The plugin shows how NDFA is used in combination of binary strings.

